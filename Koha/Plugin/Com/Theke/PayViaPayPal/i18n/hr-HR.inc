/*
	[%
		TOKENS = {
			home_breadcrumb = "Naslovnica"
			your_payment_breadcrumb = "Your Payment"
			error = "Greška"
			error_title = "problem pri obradi vašeg plaćanja"
			PAYPAL_UNABLE_TO_CONNECT = "Ne mogu se spojiti na PayPal."
			PAYPAL_TRY_LATER = "Molimo pokušajte ponovo kasnije."
			PAYPAL_ERROR_PROCESSING = "Ne mogu verificirati plaćanje."
			PAYPAL_CONTACT_LIBRARY = "Molimo kontaktirajte knjižnicu radi potvrde plaćanja."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Povratak na informacije o novčanim dugovanjima"
		}
	%]
*/