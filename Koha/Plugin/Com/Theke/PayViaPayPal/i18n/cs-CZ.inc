/*
	[%
		TOKENS = {
			home_breadcrumb = "Koha"
			your_payment_breadcrumb = "Vaše platba"
			error = "Chyba"
			error_title = "při zpracování vaší platby došlo k chybě."
			PAYPAL_UNABLE_TO_CONNECT = "Nepodařilo se připojit ke službě PayPal."
			PAYPAL_TRY_LATER = "Zkuste to prosím později."
			PAYPAL_ERROR_PROCESSING = "Nebylo možné ověřit platbu."
			PAYPAL_CONTACT_LIBRARY = "Pro ověření platby prosím kontaktujte knihovníka."
			PAYPAL_ERROR_STARTING = "Nepodařilo se zahájit proces placení."
			return = "Zpět k podrobnostem poplatku"
		}
	%]
*/