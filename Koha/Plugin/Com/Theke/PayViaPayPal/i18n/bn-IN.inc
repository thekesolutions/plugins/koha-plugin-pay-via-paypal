/*
	[%
		TOKENS = {
			home_breadcrumb = "মূল পাতা"
			your_payment_breadcrumb = "Your Payment"
			error = "ত্রুটি"
			error_title = "আপনার পেমেন্ট প্রসেসিং-এ অসুবিধা ছিলো"
			PAYPAL_UNABLE_TO_CONNECT = "Unable to connect to PayPal."
			PAYPAL_TRY_LATER = "অনুগ্রহ করে আবার চেষ্টা করুন।"
			PAYPAL_ERROR_PROCESSING = "Unable to verify payment."
			PAYPAL_CONTACT_LIBRARY = "Please contact the library to verify your payment."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Return to fine details"
		}
	%]
*/