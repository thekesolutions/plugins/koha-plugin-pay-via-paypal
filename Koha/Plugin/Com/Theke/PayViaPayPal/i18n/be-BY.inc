/*
	[%
		TOKENS = {
			home_breadcrumb = "Пачатак"
			your_payment_breadcrumb = "Your Payment"
			error = "Памылка: "
			error_title = "Была какая-то проблема с Вашей подачей"
			PAYPAL_UNABLE_TO_CONNECT = "Не удаётся добавить одну или несколько меток."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Не удаётся добавить одну или несколько меток."
			PAYPAL_CONTACT_LIBRARY = ".  Пожалуйста, обратитесь в библиотеку для получения дополнительной информации."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Вернуть этот экземпляр "
		}
	%]
*/