/*
	[%
		TOKENS = {
			home_breadcrumb = "Inici"
			your_payment_breadcrumb = "Your Payment"
			error = "Error"
			error_title = "Hi ha hagut un problema amb el procés de pagament"
			PAYPAL_UNABLE_TO_CONNECT = "Incapaç de connectar-se a PayPal."
			PAYPAL_TRY_LATER = "Si us plau, intenteu-ho més tard."
			PAYPAL_ERROR_PROCESSING = "No s'han pogut afegir les etiquetes."
			PAYPAL_CONTACT_LIBRARY = "Si us plau, contacteu amb la Biblioteca per verificar el pagament."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Torneu al detall de sancions"
		}
	%]
*/