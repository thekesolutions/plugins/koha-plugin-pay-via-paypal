/*
	[%
		TOKENS = {
			home_breadcrumb = "الصفحة الرئيسية"
			your_payment_breadcrumb = "Your Payment"
			error = "خطأ:"
			error_title = "كێشه‌یه‌ك ڕویدا له‌ ناردنه‌كه‌تدا"
			PAYPAL_UNABLE_TO_CONNECT = "نه‌توانرا یه‌كێك یان زیاتر له‌یه‌كێك له‌ تاگه‌كان زیادبكرێت."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "نه‌توانرا یه‌كێك یان زیاتر له‌یه‌كێك له‌ تاگه‌كان زیادبكرێت."
			PAYPAL_CONTACT_LIBRARY = ". تكایه‌ بۆ زانیاری زیاتر په‌یوه‌ندی به‌ كتێبخانه‌وه‌ بكه‌"
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "ئه‌م ئایتمه‌ بگه‌ڕێنه‌ره‌وه‌ "
		}
	%]
*/