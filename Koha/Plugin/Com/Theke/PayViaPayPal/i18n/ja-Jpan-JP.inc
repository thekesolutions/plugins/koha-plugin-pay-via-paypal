/*
	[%
		TOKENS = {
			home_breadcrumb = "Home"
			your_payment_breadcrumb = "Your Payment"
			error = "あげる："
			error_title = "表を手渡して途中誤りが現れる"
			PAYPAL_UNABLE_TO_CONNECT = "Unable to connect to PayPal."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Unable to verify payment."
			PAYPAL_CONTACT_LIBRARY = "代わりの連絡先情報"
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "この館所蔵を選ぶ "
		}
	%]
*/