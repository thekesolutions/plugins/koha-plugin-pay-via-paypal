/*
	[%
		TOKENS = {
			home_breadcrumb = "Domov"
			your_payment_breadcrumb = "Your Payment"
			error = "Chyba"
			error_title = "pri spracovaní vašej platby došlo k problému"
			PAYPAL_UNABLE_TO_CONNECT = "Nedá sa pripojiť na PayPal."
			PAYPAL_TRY_LATER = "Skúste, prosím, znova neskôr."
			PAYPAL_ERROR_PROCESSING = "Nie je možné overiť platbu."
			PAYPAL_CONTACT_LIBRARY = "Pre overenie vašej platby, kontaktujte prosím Vašu knižnicu."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Späť na detaily o pokutách"
		}
	%]
*/