/*
	[%
		TOKENS = {
			home_breadcrumb = "घर"
			your_payment_breadcrumb = "Your Payment"
			error = "त्रुटि"
			error_title = "वहाँ एक समस्या आपका भुगतान संसाधित में था"
			PAYPAL_UNABLE_TO_CONNECT = "PayPal से कनेक्ट करने में असमर्थ."
			PAYPAL_TRY_LATER = "कृपया फिर कोशिश करें."
			PAYPAL_ERROR_PROCESSING = "भुगतान जाँचने में असमर्थ."
			PAYPAL_CONTACT_LIBRARY = "आपके भुगतान सत्यापित करने के लिए पुस्तकालय से संपर्क करें."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "लौटें जुर्माना विवरण पर"
		}
	%]
*/