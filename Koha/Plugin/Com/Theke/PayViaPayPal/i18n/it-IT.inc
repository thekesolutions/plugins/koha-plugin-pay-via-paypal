/*
	[%
		TOKENS = {
			home_breadcrumb = "Home"
			your_payment_breadcrumb = "Your Payment"
			error = "Errore"
			error_title = "c'è un problema nel gestire il tuo pagamento"
			PAYPAL_UNABLE_TO_CONNECT = "Impossibile connettersi a PayPal"
			PAYPAL_TRY_LATER = "Vi preghiamo di riprovare più tardi"
			PAYPAL_ERROR_PROCESSING = "Impossibile verificare il pagamento."
			PAYPAL_CONTACT_LIBRARY = "Per favore contatta la biblioteca per verificare il tuo pagamento."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Ritorna ai dettagli della multa"
		}
	%]
*/