/*
	[%
		TOKENS = {
			home_breadcrumb = "Início"
			your_payment_breadcrumb = "Your Payment"
			error = "Erro"
			error_title = "ocorreu um problema ao processar seu pagamento"
			PAYPAL_UNABLE_TO_CONNECT = "Não é possível se conectar ao PayPal."
			PAYPAL_TRY_LATER = "Por favor, tente novamente mais tarde."
			PAYPAL_ERROR_PROCESSING = "Não é possível confirmar o pagamento."
			PAYPAL_CONTACT_LIBRARY = "Entre em contato com a biblioteca para verificar seu pagamento."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Retornar para detalhes"
		}
	%]
*/