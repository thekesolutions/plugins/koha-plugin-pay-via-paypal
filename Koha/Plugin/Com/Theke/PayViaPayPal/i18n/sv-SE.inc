/*
	[%
		TOKENS = {
			home_breadcrumb = "Hem"
			your_payment_breadcrumb = "Your Payment"
			error = "Fel "
			error_title = "ett problem uppstod med att processa din betalning"
			PAYPAL_UNABLE_TO_CONNECT = "Kan inte ansluta till PayPal. "
			PAYPAL_TRY_LATER = "Vänligen försök på nytt senare. "
			PAYPAL_ERROR_PROCESSING = "Kan inte bekräfta betalningen. "
			PAYPAL_CONTACT_LIBRARY = "Vänligen kontakta biblioteket för att bekräfta din betalning. "
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Återgå till vy för avgifter"
		}
	%]
*/