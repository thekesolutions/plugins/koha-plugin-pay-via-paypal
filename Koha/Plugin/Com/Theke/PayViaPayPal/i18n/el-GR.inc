/*
	[%
		TOKENS = {
			home_breadcrumb = "Αρχική"
			your_payment_breadcrumb = "Your Payment"
			error = "Σφάλμα"
			error_title = "υπήρξε πρόβλημα κατά τη διαδικασία πληρωμής"
			PAYPAL_UNABLE_TO_CONNECT = "Αδυναμία σύνδεσης με το PayPal."
			PAYPAL_TRY_LATER = "Προσπαθήστε ξανά αργότερα."
			PAYPAL_ERROR_PROCESSING = "Αδυναμία επιβεβαίωσης της πληρωμής."
			PAYPAL_CONTACT_LIBRARY = "Παρακαλώ επικοινωνήστε με τη βιβλιοθήκη για να επιβεβαιώσετε την πληρωμή σας."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Επιστροφή στις λεπτομέρειες προστίμων"
		}
	%]
*/