/*
	[%
		TOKENS = {
			home_breadcrumb = "Persoonlijke map"
			your_payment_breadcrumb = "Your Payment"
			error = "Fout"
			error_title = "er was een probleem met de verwerking van uw betaling"
			PAYPAL_UNABLE_TO_CONNECT = "Niet in staat om verbinding te maken met PayPal."
			PAYPAL_TRY_LATER = "Probeer het alstublieft later opnieuw."
			PAYPAL_ERROR_PROCESSING = "Niet in staat om om betaling te bevestigen."
			PAYPAL_CONTACT_LIBRARY = "Neem contact op met de bibliotheek om uw betaling te verifiëren."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Keer terug naar boetedetails"
		}
	%]
*/