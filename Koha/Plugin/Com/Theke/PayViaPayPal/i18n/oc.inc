/*
	[%
		TOKENS = {
			home_breadcrumb = "Acuèlh"
			your_payment_breadcrumb = "Your Payment"
			error = "Error&nbsp;:"
			error_title = "I a eu un problèma amb votre validation"
			PAYPAL_UNABLE_TO_CONNECT = "Impossible d'apondre des tags."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Impossible d'apondre des tags."
			PAYPAL_CONTACT_LIBRARY = ". Contactatz la bibliotèca per mai d'informacions."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Tornar a "
		}
	%]
*/